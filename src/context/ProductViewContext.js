import React, { createContext, useContext, useState } from 'react';

const ProductViewContext = createContext();

export const useProductView = () => {
    return useContext(ProductViewContext);
};

export const ProductViewProvider = ({ children }) => {
    const [isGridView, setIsGridView] = useState(true);

    const toggleView = () => {
        setIsGridView(prevValue => !prevValue);
    };

    return (
        <ProductViewContext.Provider value={{ isGridView, toggleView }}>
            {children}
        </ProductViewContext.Provider>
    );
};
