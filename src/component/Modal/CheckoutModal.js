import React, { useEffect, useRef } from "react";

const CheckoutModal = ({ isOpen, setIsOpen, onGoToOrder, onGoToCatalog }) => {
    const modalRef = useRef(null);

    const closeModal = () => {
        setIsOpen(false);
    };

    useEffect(() => {
        const handleClickOutside = (event) => {
            if (modalRef.current && !modalRef.current.contains(event.target)) {
                closeModal();
            }
        };

        const handleEscapeKey = (event) => {
            if (event.key === "Escape") {
                closeModal();
            }
        };

        if (isOpen) {
            document.addEventListener("mousedown", handleClickOutside);
            document.addEventListener("keydown", handleEscapeKey);
        } else {
            document.removeEventListener("mousedown", handleClickOutside);
            document.removeEventListener("keydown", handleEscapeKey);
        }

        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
            document.removeEventListener("keydown", handleEscapeKey);
        };
    }, [isOpen]);

    if (!isOpen) {
        return null;
    }

    return (
        <div className="checkout-modal-overlay">
            <div className="checkout-modal" ref={modalRef}>
                <button className="close-button" onClick={closeModal}>
                    &times;
                </button>
                <p>Ваше замовлення прийняте</p>
                <button className="btn-checkout" onClick={onGoToOrder}>
                    Перейти до замовлення
                </button>
                <button className="btn-checkout" onClick={onGoToCatalog}>
                    Перейти до каталогу
                </button>
            </div>
        </div>
    );
};

export default CheckoutModal;
    