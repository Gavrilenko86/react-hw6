import React from 'react';
import { render, screen } from '@testing-library/react';
import Modal from './Modal';

test('Modal displays correctly with header, text, and buttons', () => {
    const { getByText } = render(
        <Modal header="Test Modal" text="This is a test modal">
            <button className='btn-cancel'>CANCEL</button>
            <button className='btn-ok'>OK</button>
        </Modal>
    );

    expect(screen. getByText('Test Modal')).toBeInTheDocument();
    expect(screen.getByText('This is a test modal')).toBeInTheDocument();
    expect(screen.getByText('CANCEL')).toBeInTheDocument();
    expect(screen.getByText('OK')).toBeInTheDocument();
});
