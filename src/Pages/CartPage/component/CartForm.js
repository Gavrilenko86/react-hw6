import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import "./CartForm.scss";

const CheckoutForm = ({ onSubmit }) => {
    const initialValues = {
        firstName: "",
        lastName: "",
        age: "",
        address: "",
        mobile: "",
    };

    const validationSchema = Yup.object({
        firstName: Yup.string().required("Це поле обов'язкове для заповнення"),
        lastName: Yup.string().required("Це поле обов'язкове для заповнення"),
        age: Yup.number()
            .typeError("Повинно бути числом")
            .required("Це поле обов'язкове для заповнення"),
        address: Yup.string().required("Це поле обов'язкове для заповнення"),
        mobile: Yup.string().required("Це поле обов'язкове для заповнення"),
    });

    const handleSubmit = (values) => {
        onSubmit(values);
    };

    return (
        <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
        >
            <Form className="form-container">
                <fieldset className="form-block">
                    <legend>User block</legend>
                    <div className="form-content">
                        <label className="first-name" htmlFor="firstName">First Name:</label>
                        <Field className="input-style" type="text" id="firstName" name="firstName" />
                        <ErrorMessage className="error-value" name="firstName" component="div" />
                    </div>

                    <div className="form-content">
                        <label className="last-name" htmlFor="lastName">Last Name:</label>
                        <Field className="input-style" type="text" id="lastName" name="lastName" />
                        <ErrorMessage className="error-value" name="lastName" component="div" />
                    </div>

                    <div className="form-content">
                        <label className="age" htmlFor="age">Age:</label>
                        <Field className="input-style" type="number" id="age" name="age" />
                        <ErrorMessage className="error-value" name="age" component="div" />
                    </div>

                    <div className="form-content">
                        <label className="address" htmlFor="address">Address:</label>
                        <Field className="input-style" type="text" id="address" name="address" />
                        <ErrorMessage className="error-value" name="address" component="div" />
                    </div>

                    <div className="form-content">
                        <label className="mobile" htmlFor="mobile">Mobile number:</label>
                        <Field className="input-style" type="text" id="mobile" name="mobile" />
                        <ErrorMessage className="error-value" name="mobile" component="div" />
                    </div>
                </fieldset>

                <button className="btn-cart" type="submit">Checkout</button>
            </Form>
        </Formik>
    );
};

export default CheckoutForm;
