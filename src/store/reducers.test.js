import rootReducer from './reducers.js'; 
import { actionProductList, actionModalOpen, actionModalClose } from './actions';

test('reducer handles actionProductList correctly', () => {
    const initialState = {
        productList: [],
    };
    const action = actionProductList([{ id: 1, name: 'Product 1' }]);
    const newState = rootReducer(initialState, action);
    expect(newState.productList).toEqual([{ id: 1, name: 'Product 1' }]);
});

test('reducer handles actionModalOpen correctly', () => {
    const initialState = {
        isModalOpen: false,
    };
    const action = actionModalOpen();
    const newState = rootReducer(initialState, action);
    expect(newState.isModalOpen).toBe(true);
});

test('reducer handles actionModalClose correctly', () => {
    const initialState = {
        isModalOpen: true,
    };
    const action = actionModalClose();
    const newState = rootReducer(initialState, action);
    expect(newState.isModalOpen).toBe(false);
});
